import sqlite3

class Database:
    def __init__(self, name):
        self.db = sqlite3.connect(name)

    def __del__(self):
        self.db.close()

    def drop_table(self, table_name):
        cur = self.db.cursor()
        cur.execute(f"DROP TABLE IF EXISTS {table_name}")
        self.db.commit()

    def create_table(self, table_name, schema):
        cur = self.db.cursor()
        cur.execute(f"CREATE TABLE IF NOT EXISTS {table_name} ({schema})")
        self.db.commit()

    def load_table(self, table_name, df):
        df = self._clean_columns(df)
        df.to_sql(table_name, self.db, if_exists="append", index=False)
    
    @staticmethod
    def _clean_columns(df):
        df.columns = df.columns.str.lstrip().str.replace(" ", "_")
        return df

    @staticmethod
    def generate_schema(df):
        columns = df.columns
        query_args = ""
        for column in columns:
            column = column.lstrip()
            column = column.replace(" ", "_")
            if query_args == "":
                query_args = f"[{column}] text"
            else:
                query_args += f", [{column}] text"
        return query_args
