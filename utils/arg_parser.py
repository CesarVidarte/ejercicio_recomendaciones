import argparse


def create_parser():
    parser = argparse.ArgumentParser(description="arguments for anime DB creation")
    parser.add_argument(
        "-t",
        "--type",
        dest="type",
        type=str,
        help="Load Type (Incremental, Bulkload)",
        required=True,
        action=CheckTypeAction,
    )
    parser.add_argument(
        "-i",
        "--input_path",
        dest="input_path",
        type=str,
        help="path to the csv files",
        required=True,
    )
    parser.add_argument(
        "-d",
        "--dest_table_name",
        dest="table_name",
        type=str,
        help="name of the table",
        required=True,
        action=CheckTablesAction,
    )
    parser.add_argument(
        "-s",
        "--split",
        dest="split",
        type=str,
        help="do you want to split?",
        required=False,
    )
    parser.add_argument(
        "-n",
        "--n_split",
        dest="n_split",
        type=int,
        help="number of splits",
        required=False,
        default=3,
    )
    parser.add_argument(
        "-o",
        "--output_path",
        dest="output_path",
        type=str,
        help="folder to save the chunks",
        required=False,
    )
    return parser


class CheckTypeAction(argparse.Action):
    def __call__(
        self, parser, namespace, values, option_string=None
    ):  # pylint: disable=unused-argument
        if values.lower() not in ["inc", "bulk", "split"]:
            raise ValueError(
                "Not a valid process Inc, Bulk, split"
            )
        setattr(namespace, self.dest, values.lower())


class CheckTablesAction(argparse.Action):
    def __call__(
        self, parser, namespace, values, option_string=None
    ):  # pylint: disable=unused-argument
        if values.lower() not in ["anime", "animelist", "anime_recommendations", "watching_status", "rating_complete"]:
            raise ValueError(
                "Not a valid Table, anime, animelist, anime_recommendations, watching_status, rating_complete"
            )
        setattr(namespace, self.dest, values.lower())


def create_parser_split():
    parser = argparse.ArgumentParser(description="arguments for splitting a csv")
    parser.add_argument(
        "-n",
        "--n_split",
        dest="n_split",
        type=int,
        help="n_split",
        required=False,
        default=3,
    )
    parser.add_argument(
        "-p",
        "--split_table",
        dest="split_table",
        type=str,
        help="path to the .csv to split",
        required=True,
    )
    return parser
