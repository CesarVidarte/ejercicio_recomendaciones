import pandas as pd
import re


def _read_csv(path: str, sep=",", low_memory=False, dtype="str", encoding="utf-8"):
    return pd.read_csv(
        path, low_memory=low_memory, dtype=dtype, sep=sep, encoding=encoding
    )


def _get_table_name(path: str):
    x = re.search(r"/(.+).csv", path).group(1)
    return x
