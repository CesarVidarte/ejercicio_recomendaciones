help:
	@echo "This process runs for ccpa"
	@echo "Remember to use all the arguments"

clean: clean-build clean-pyc clean-libs

clean-all: clean clean-venv clean

clean-build:
	rm -fr dist/

clean-libs:
	rm -fr libs/

clean-pyc:
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +
	find . -name '*~' -exec rm -f {} +
	find . -name '__pycache__' -exec rm -fr {} +
	clear


clean-venv:
	rm -fr ./venv

venv:
	virtualenv venv
	venv/bin/python3.8 -m pip install -r requirements.txt