import numpy as np
import re


def _anime_df(df):
    df.drop_duplicates(subset=["Anime-PlanetID", "Name"], inplace=True)
    df["Name"] = np.where(
        df["Name"].str.find("[email protected]") != -1,
        df["Url"]
        .apply(lambda x: re.search(r"anime/(.+)", x).group(1))
        .str.replace("-", " "),
        df["Name"],
    )
    df["Alternative Name"] = np.where(
        df["Alternative Name"].str.find("[email protected]") != -1,
        "Unknown",
        df["Alternative Name"],
    )
    return df


def _anime_recomm_df(df):
    df = df.drop_duplicates(subset=["Anime", "Recommendation"])
    return df
    # return df.drop_duplicates(subset=["Anime", "Recommendation"], inplace=True)


def _anime_list_df(df):
    return df.sort_values(by=["watching_status"]).drop_duplicates(
        subset=["user_id", "anime_id"], keep="first"
    )


def _rating_df(df):
    return df


def _watching_s_df(df):
    return df
