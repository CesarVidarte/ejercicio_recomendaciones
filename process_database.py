from utils.arg_parser import create_parser
import modules.standarize as stand
from split import run as srun
from utils import utils_db as db
import pandas as pd


cleaning = {
    "anime": stand._anime_df,
    "animelist": stand._anime_list_df,
    "anime_recommendations": stand._anime_recomm_df,
    "watching_status": stand._watching_s_df,
    "rating_complete": stand._rating_df
}


DB_NAME = "Anime.db"


def load_data(args):
    Db = db.Database(DB_NAME)
    df = pd.read_csv(args.input_path)
    df = cleaning[args.table_name](df)
    if args.type == "bulk":
        Db.drop_table(args.table_name)
    Db.create_table(args.table_name, Db.generate_schema(df))
    Db.load_table(args.table_name, df)


if __name__ == "__main__":
    parser = create_parser()
    args = parser.parse_args()

    if args.type != "split":
        load_data(args)
    else:
        try:
            srun(args.input_path, args.n_split)
        except FileNotFoundError as errno:
            print(repr(errno))