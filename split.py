from utils.arg_parser import create_parser_split
from utils import ST_INCRE
import utils.utils as ut
import numpy as np
import os


def run(table_path, n_split):
    try:
        anime_df = ut._read_csv(table_path)
        dfs = np.array_split(anime_df, n_split)
        table_name = ut._get_table_name(table_path)
        if not os.path.exists(ST_INCRE):
            os.makedirs(ST_INCRE)
        for i in range(len(dfs)):
            dfs[i].to_csv(ST_INCRE + table_name + f"_part{i+1}" + ".csv", sep=",", index=False)
    except FileNotFoundError as file_error:
        raise FileNotFoundError("input path '%s' does not exist: %s" % (table_path, file_error))


if __name__ == "__main__":
    parser = create_parser_split()
    args = parser.parse_args()
    run(args.split_table, args.n_split)
